## Test technique d'Alexandre Guedj pour Sebastien Horin

### Stack technique

- Utilisation de Vite + template React ;
- TailwindCSS et Sass pour la partie CSS.

### Réponse technique apportée à la problématique

Le menu dynamique est créé en se basant sur un format JSON (disponible dans le fichier `src/config/menu.js`).
Pour chaque niveau du menu, le composant `MenuItemsList` itère parmi les éléments du menu, en rendant chaque élément avec le composant `MenuItem`. Si l'élément du menu rendu dans le composant `MenuItem` possède des enfants (des éléments dans le noeud `children`), alors un nouveau composant `MenuItemsList` est appelé, rendant ainsi les enfants de l'élément.

Cette manière de répondre au problème de manière récursive permet d'avoir autant de niveau que l'on souhaite dans le menu.

### Lancer le projet

Depuis le terminal, lancer la commande `yarn install` pour installer les dépendances, puis `vite` pour lancer le serveur de développement.

### Rendu final en ligne

Le rendu peut être visualisé directement sur l'URL https://rococo-pithivier-ce73ec.netlify.app/.
