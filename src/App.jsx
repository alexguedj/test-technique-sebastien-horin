import { useState, useEffect } from 'react'
import MenuItemsList from './components/MenuItemsList'
import Menu from './config/menu.js'
import ToggleThemeButton from './components/ToggleThemeButton.jsx'
import JsonPrettier from './components/JsonPrettier.jsx'

function App() {
  const [activeDepth, setActiveDepth] = useState(0)
  const [isActive, setIsActive] = useState(true)
  const [height, setHeight] = useState(0)
  
  useEffect(() => {
    setHeight(document.querySelector('.items-list.active').offsetHeight)
  }, [])
  
  const onChangeDepth = (depth) => {
    setActiveDepth(depth)
    setTimeout(() => {
      setHeight(document.querySelector('.items-list.active').offsetHeight)
    }, 10)
  }

  return (
    <div className="App">
      <ToggleThemeButton />
      <div className="grid grid-cols-1 md:grid-cols-2">
        <div className="json-container">
          <JsonPrettier className="json-model"
                        data={Menu} />
        </div>
        <div className="flex items-center justify-center order-1 md:order-2 h-screen">
          <div className="items-list-container"
               style={{height}}>
            <MenuItemsList items={Menu}
                           isActive={isActive}
                           setIsActive={setIsActive}
                           style={{transform: `translateX(${-100*activeDepth}%)`}}
                           onChangeDepth={(depth) => onChangeDepth(depth)}
                           depth={0} />
          </div>
        </div>
      </div>
    </div>
  )
}

export default App
