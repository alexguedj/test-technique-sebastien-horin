const menu = [
  {
    label: 'Mode',
    children: [
      {
        label: 'Prêt à porter',
        link: 'https://www.chanel.com/fr/mode/pret-a-porter/'
      },
      {
        label: 'Sacs',
        link: 'https://www.chanel.com/fr/mode/sacs/c/1x1x1/'
      },
      {
        label: 'Souliers',
        link: 'https://www.chanel.com/fr/mode/souliers/c/1x1x5/'
      },
      {
        label: 'Bijoux',
        link: 'https://www.chanel.com/fr/mode/bijoux/c/1x1x3/'
      }
    ]
  },
  {
    label: 'Parfums',
    children: [
      {
        label: 'Femmes',
        children: [
          {
            label: 'N°5',
            link: 'https://www.chanel.com/fr/parfums/femmes/c/7x1x1x30/n5/'
          },
          {
            label: 'Coco Mademoiselle',
            link: 'https://www.chanel.com/fr/parfums/femmes/c/7x1x1x31/coco-mademoiselle/'
          },
          {
            label: 'Chance',
            link: 'https://www.chanel.com/fr/parfums/femmes/c/7x1x1x32/chance/'
          },
          {
            label: 'Gabrielle',
            link: 'https://www.chanel.com/fr/parfums/femmes/c/7x1x1x40/gabrielle-chanel/'
          }
        ]
      },
      {
        label: 'Hommes',
        children: [
          {
            label: 'Allure Homme Sport',
            link: 'https://www.chanel.com/fr/parfums/hommes/c/7x1x2x32/allure-homme-sport/'
          },
          {
            label: 'Égoïste',
            link: 'https://www.chanel.com/fr/parfums/hommes/c/7x1x2x35/egoiste/'
          },
          {
            label: 'Antaeus',
            link: 'https://www.chanel.com/fr/parfums/hommes/c/7x1x2x37/antaeus/'
          }
        ]
      },
      {
        label: 'Bain et corps',
        children: [
          {
            label: 'Femmes',
            link: 'https://www.chanel.com/fr/parfums/bain-et-corps/c/7x1x7x92/femmes/'
          },
          {
            label: 'Hommes',
            link: 'https://www.chanel.com/fr/parfums/bain-et-corps/c/7x1x7x93/hommes/'
          },
          {
            label: 'Tout voir',
            link: 'https://www.chanel.com/fr/parfums/bain-et-corps/c/7x1x7/'
          }
        ]
      }
    ]
  },
  {
    label: 'Maquillage',
    children: [
      {
        label: 'Teint',
        children: [
          {
            label: 'Fonds de teint',
            link: 'https://www.chanel.com/fr/maquillage/teint/c/5x1x6x30/fonds-de-teint/'
          },
          {
            label: 'Bases',
            link: 'https://www.chanel.com/fr/maquillage/teint/c/5x1x6x31/bases/'
          },
          {
            label: 'Correcteurs',
            link: 'https://www.chanel.com/fr/maquillage/teint/c/5x1x6x36/correcteurs/'
          }
        ]
      },
      {
        label: 'Yeux',
        children: [
          {
            label: 'Mascaras',
            link: 'https://www.chanel.com/fr/maquillage/yeux/c/5x1x4x31/mascaras/'
          },
          {
            label: 'Ombres à Paupières',
            link: 'https://www.chanel.com/fr/maquillage/yeux/c/5x1x4x30/ombres-a-paupieres/'
          },
          {
            label: 'Contour des yeux',
            link: 'https://www.chanel.com/fr/maquillage/yeux/c/5x1x4x33/contour-des-yeux/'
          }
        ]
      },
      {
        label: 'Ongles',
        children: [
          {
            label: 'Vernis à ongles',
            link: 'https://www.chanel.com/fr/maquillage/ongles/c/5x1x7x31/vernis-a-ongles/'
          },
          {
            label: 'Manucure',
            link: 'https://www.chanel.com/fr/maquillage/ongles/c/5x1x7x30/manucure/'
          }
        ]
      }
    ]
  },
  {
    label: 'About Chanel',
    link: 'https://www.chanel.com/fr/about-chanel/l-histoire/'
  },
]

export default menu
