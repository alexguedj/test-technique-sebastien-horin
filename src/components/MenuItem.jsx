import { useState } from 'react'
import MenuItemsList from './MenuItemsList'

const MenuItem = ({ item, depth, toggleParentListIsActive, onChangeDepth }) => {
  const [isChildMenuActive, setIsChildMenuActive] = useState(false)
  
  const displayChildren = () => {
    if (item.children) {
      setIsChildMenuActive(true)
      onChangeDepth(depth + 1)
      if (toggleParentListIsActive) {
        toggleParentListIsActive(false)
      }
    }
  }
  const onBackEnd = () => {
    setIsChildMenuActive(false)
  }
  return (
    <li className="flex w-96">
      <a className={`list-item ${item.children ? 'has-children' : ''}`}
         target="_blank"
         href={item.link}
         onClick={displayChildren}>
        {item.label}
      </a>
      {item.children && isChildMenuActive &&
        <MenuItemsList items={item.children}
                       toggleParentListIsActive={toggleParentListIsActive}
                       onBackEnd={onBackEnd}
                       onChangeDepth={onChangeDepth}
                       depth={depth + 1} />
      }
    </li>
  )
}

export default MenuItem
