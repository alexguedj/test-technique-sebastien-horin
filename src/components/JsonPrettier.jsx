const JsonPrettier = ({ className, data }) => {
  return (
    <div className={className}>
      <pre className="text-sm">
        {JSON.stringify(data, null, 2)}
      </pre>
    </div>
  )
}

export default JsonPrettier
