import { useState } from 'react'
import MenuItem from './MenuItem'

const MenuItemsList = ({ items, depth, onBackEnd, onChangeDepth, toggleParentListIsActive, style }) => {
  const [isActive, setIsActive] = useState(true)
  const goBack = () => {
    onChangeDepth(depth - 1)
    setIsActive(false)
    if (toggleParentListIsActive) {
      toggleParentListIsActive(true)
    }
    setTimeout(() => {
      onBackEnd()
    }, 500)
  }

  return (
    <ul className={`items-list ${isActive ? 'active' : ''} ${depth ? 'absolute right-0 top-0 transform translate-x-full' : 'relative'}`}
        style={style}>
      {!!depth &&
        <li>
          <a className="list-item list-item--back"
             onClick={goBack} />
        </li>
      }
      {items.map((item, index) =>
        (
          <MenuItem key={index}
                    toggleParentListIsActive={setIsActive}
                    depth={depth}
                    onChangeDepth={onChangeDepth}
                    item={item} />
        ))
      }
    </ul>
  )
}

export default MenuItemsList
